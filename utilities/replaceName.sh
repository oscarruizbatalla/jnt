#!/usr/bin/ksh


# Replace tool

USAGE="replaceName <old_name> <new_name>"

if [[ $# -ne 2 ]]
then
	echo $USAGE
	exit
fi

echo "Replacing word $1 by $2"

echo "#!/usr/bin/ksh" > modify.sh

for i in $(find . -name "*.java")
do
	
	HAS_FORMER=$(cat $i | grep $1)	
	if [[ $HAS_FORMER != "" ]]
	then
		echo "Replacing $i ..."
		echo "    found line: $HAS_FORMER"
		
		echo "echo \"Modify file $i\"" >> modify.sh
		echo "cat $i | sed 's/$1/$2/g' > $i.new" >> modify.sh
		echo "mv $i.new $i" >> modify.sh
	fi
done
