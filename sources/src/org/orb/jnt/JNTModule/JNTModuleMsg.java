package org.orb.jnt.JNTModule;

import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTQueue.*;

public class JNTModuleMsg 
{

  public int       operation;
  public JNTModule moduleID;
  public JNTQueue  queue;
  public JNTTimer  timer;
  public byte[] message;

  public JNTModuleMsg(JNTModule moduleID, int operation, JNTTimer timer) 
  {
    this.moduleID  = moduleID;
    this.operation = operation;
    this.timer     = timer;
  }

  public JNTModuleMsg(JNTModule moduleID, int operation, JNTQueue queue)
  {
    this.moduleID  = moduleID;
    this.operation = operation;
    this.queue     = queue;
  }
  
  public JNTModuleMsg(JNTModule moduleID, int operation, byte[] msg)
  {
    this.moduleID  = moduleID;
    this.operation = operation;
    this.message = msg;
  }
}
