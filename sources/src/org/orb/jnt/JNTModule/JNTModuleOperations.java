package org.orb.jnt.JNTModule;


public class JNTModuleOperations
{
  // queues
  public static final int BOTTOM_QUEUE_OPENED = 0; 
  public static final int BOTTOM_QUEUE_CLOSED = 1;
  public static final int BOTTOM_QUEUE_ACCEPTED = 2;
  public static final int BOTTOM_QUEUE_REJECTED = 3;
  public static final int BOTTOM_QUEUE_SERVICE = 4;
  
  public static final int TOP_QUEUE_OPENED = 10;
  public static final int TOP_QUEUE_CLOSED = 11;
  public static final int TOP_QUEUE_ACCEPTED = 12;
  public static final int TOP_QUEUE_REJECTED = 13;
  public static final int TOP_QUEUE_SERVICE = 14;
  
    public static final int CONNECTION_SERVICE = 15;

  // messages
  public static final int TIMER_EXPIRED = 20;

  public JNTModuleOperations() {}
}
