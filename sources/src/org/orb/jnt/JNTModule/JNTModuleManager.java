package org.orb.jnt.JNTModule;

//import java.io.*;
import java.util.LinkedList;
import java.util.Hashtable;
import java.util.Enumeration;

import org.apache.log4j.Logger;

import org.orb.jnt.JNTAdapter.JNTAdapter;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTTimer.*;


public class JNTModuleManager 
{
  // attributes
  // ----------------------------------------------------------
  public static JNTModule currentmodule;

  public static Hashtable moduleList;
  public static LinkedList msgList;

  // debugger
  static Logger deb = Logger.getLogger(JNTModuleManager.class.getName());

  // member functions
  //  ----------------------------------------------------------
  public JNTModuleManager() {}
  
  public static void Initialize()
  {
    deb.debug("Initialize");

    moduleList = new Hashtable();
    msgList = new LinkedList();
  }
  
  public static void Finalize()
  {
    deb.debug("Finalize");

    if (null == moduleList)
    {
      deb.warn("Null moduleList");
      return;
    }
    
    for(Enumeration e = moduleList.keys(); e.hasMoreElements(); )
    {
    	String moduleName = (String) e.nextElement();
    	JNTModule module = (JNTModule) moduleList.get(moduleName);
    	
    	module.Finalize();
    }    
 
    moduleList.clear();
  }

  public static void AllocAndQueueMessage(JNTModuleMsg msg)
  throws JNTException
  {
    deb.debug("AllocAndQueueMessage");

    if (null == msgList)
    {
      deb.fatal("Throw null pointer exception");
      throw new JNTNullPointerException("JNTModuleManager::AllocAndQueueMessage");
    }

    try
    {
      msgList.addLast(msg);
    }
    catch (Exception e)
    {  
      deb.warn("Throw fail exception");
      throw new JNTFailException("JNTModuleManager::AllocAndQueueMessage");
    }
  }

  public static void Register(JNTModule module, String id)
  throws JNTException
  {
    deb.debug("Register");

    if (null == msgList)
    {
      deb.fatal("Throw null pointer exception");
      throw new JNTNullPointerException("JNTModuleManager::Register");
    }

    if (null == moduleList)
    {
      deb.fatal("Null moduleList");
      throw new JNTNullPointerException("JNTModuleManager::Register");
    }

    if (moduleList.containsKey(id))
    {
      deb.warn("moduleList does not contain given module");
      throw new JNTFailException("JNTModuleManager::Register");
    }
    
    try
    {
      module.SetID(id);
      moduleList.put(id, module);
    }
    catch (Exception e)
    {
      deb.warn("Could not add module to list, throw fail exception");
      throw new JNTFailException("JNTModuleManager::Register");
    }
   
    currentmodule = module; 
  
    // start module
    currentmodule.Initialize();
  }

  public static void UnRegister(JNTModule module)
  throws JNTException
  {
    deb.debug("UnRegister");

    if (null == msgList)
    {
      deb.fatal("Throw null pointer exception");
      throw new JNTNullPointerException("JNTModuleManager::UnRegister");
    }

    if (!moduleList.containsKey(module.GetID()))
    {
      deb.warn("moduleList does not contain given module");
      throw new JNTFailException("JNTModuleManager::UnRegister");
    }

    try 
    {
      moduleList.remove(module.GetID());
    }
    catch (Exception e)
    {
      deb.warn("Cound not unregister module");
      throw new JNTFailException("JNTModuleManager::UnRegister");
    }
  }

  public static void Signal(JNTModule moduleID, int operation, JNTQueue queue) 
  throws JNTException
  {
    deb.debug("Signal - queue");
  
    try
    {
      JNTModuleMsg msg = new JNTModuleMsg(moduleID, operation, queue);
      AllocAndQueueMessage(msg);
    }
    catch (JNTException e)
    {
      deb.warn("Could not allocate and queue message");
      throw e;
    }
  }
  
  public static void Signal(JNTModule moduleID, int operation, byte[] message) 
  throws JNTException
  {
    deb.debug("Signal - queue");
  
    try
    {
      JNTModuleMsg msg = new JNTModuleMsg(moduleID, operation, message);
      AllocAndQueueMessage(msg);
    }
    catch (JNTException e)
    {
      deb.warn("Could not allocate and queue message");
      throw e;
    }
  }

  public static void Signal(JNTModule moduleID, int operation, JNTTimer timer)
  throws JNTException
  {
    deb.debug("Signal - timer");

    try
    {
      JNTModuleMsg msg = new JNTModuleMsg(moduleID, operation, timer);
      AllocAndQueueMessage(msg);
    }
    catch (JNTException e)
    {
      deb.warn("Could not allocate and queue message");
      throw e;
    }
  }

  public static boolean IsValid(JNTModule moduleID)
  {
    return moduleList.containsKey(moduleID.GetID());
  }

  public static JNTModule GetModule(String id)
  {
    if (null == moduleList)
    {
      return null;
    }
    
    JNTModule module = (JNTModule) moduleList.get(id);
    
    return module;    
  }

  public static void ProcessMessages()
  {
    JNTModuleMsg msg;

    while (0 != msgList.size())
    {
      
      msg = (JNTModuleMsg) msgList.removeFirst();
      currentmodule = msg.moduleID;

      if (!IsValid(currentmodule))
      {
        return;
      }
  
      switch (msg.operation)   
      {
        case JNTModuleOperations.BOTTOM_QUEUE_OPENED:
          currentmodule.BottomQueueOpened(msg.queue);
          break;
        case JNTModuleOperations.BOTTOM_QUEUE_CLOSED:
          currentmodule.BottomQueueClosed(msg.queue);
          break;
        case JNTModuleOperations.BOTTOM_QUEUE_ACCEPTED:
          currentmodule.BottomQueueAccepted(msg.queue);
          break;
        case JNTModuleOperations.BOTTOM_QUEUE_REJECTED:
          currentmodule.BottomQueueRejected(msg.queue);
          break;
        case JNTModuleOperations.BOTTOM_QUEUE_SERVICE:
          currentmodule.BottomQueueService(msg.queue);
          break;
        case JNTModuleOperations.TOP_QUEUE_OPENED:
          currentmodule.TopQueueOpened(msg.queue);
          break;
        case JNTModuleOperations.TOP_QUEUE_CLOSED:
          currentmodule.TopQueueClosed(msg.queue);
          break;
        case JNTModuleOperations.TOP_QUEUE_ACCEPTED:
          currentmodule.TopQueueAccepted(msg.queue);
          break;
        case JNTModuleOperations.TOP_QUEUE_REJECTED:
          currentmodule.TopQueueRejected(msg.queue);
          break;
        case JNTModuleOperations.TOP_QUEUE_SERVICE:
          currentmodule.TopQueueService(msg.queue);
          break;
        case JNTModuleOperations.TIMER_EXPIRED:
          currentmodule.TimerExpired(msg.timer);
          break;
        case JNTModuleOperations.CONNECTION_SERVICE:
          JNTAdapter adapterModule = (JNTAdapter) currentmodule;
          adapterModule.ConnectionService(msg.message);
          break;
        default:        
          deb.fatal("Unknown operation " + msg.operation);
          break;
      }
      
    }
  }
}
