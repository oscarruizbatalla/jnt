package org.orb.jnt.JNTModule;

import java.lang.*;
import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTQueue.*;

public abstract class JNTModule 
{
  // attributes
  // ------------------------------------------------------

  public String id;

  // member functions 
  // ------------------------------------------------------

  public JNTModule() 
  {
    id = "";
  }

  public void SetID(String id)
  {
    this.id = id;
  }
  
  public String GetID()
  {
    return this.id;
  }

  public static void sleep(int secs)
  {
    try { Thread.sleep(secs*1000); }
    catch (InterruptedException e) {}
  }

  // abstract member functions
  // ------------------------------------------------------
  
  // initializer
  public abstract void Initialize();

  // finalizer
  public abstract void Finalize();

  // timers
  public abstract void TimerExpired(JNTTimer timer);
  
  // Top queue
  public abstract void TopQueueOpened(JNTQueue queue);
  public abstract void TopQueueClosed(JNTQueue queue);
  public abstract void TopQueueAccepted(JNTQueue queue);
  public abstract void TopQueueRejected(JNTQueue queue);
  public abstract void TopQueueService(JNTQueue queue);

  // Bottom queue
  public abstract void BottomQueueOpened(JNTQueue queue);
  public abstract void BottomQueueClosed(JNTQueue queue);
  public abstract void BottomQueueAccepted(JNTQueue queue);
  public abstract void BottomQueueRejected(JNTQueue queue);
  public abstract void BottomQueueService(JNTQueue queue);
}
