package org.orb.jnt.JNTModule;

import org.orb.jnt.JNTQueue.JNTQueue;
import org.orb.jnt.JNTTimer.JNTTimer;

public abstract class JNTSTModule extends JNTModule implements Runnable
{
    private Thread runner = null;
    
    private boolean ALIVE = false;
    
    public JNTSTModule()
    {
    }
    
    public void Initialize()
    {
        if (null == runner || !runner.isAlive())
        {
            runner = new Thread(this);
            runner.start();
        }
        
        STInitialize();
        
    }
    
    public void Finalize()
    {
        ALIVE = false;        
    }
    
    
    public void run() 
    {
        ALIVE = true;
        while (ALIVE)
        {                    
            Service();
        }
    }
    
    public abstract void STInitialize();
    public abstract void Service();

    // finalizer
    public abstract void STFinalize();

    // timers
    public abstract void TimerExpired(JNTTimer timer);
    
    // Top queue
    public abstract void TopQueueOpened(JNTQueue queue);
    public abstract void TopQueueClosed(JNTQueue queue);
    public abstract void TopQueueAccepted(JNTQueue queue);
    public abstract void TopQueueRejected(JNTQueue queue);
    public abstract void TopQueueService(JNTQueue queue);

    // Bottom queue
    public abstract void BottomQueueOpened(JNTQueue queue);
    public abstract void BottomQueueClosed(JNTQueue queue);
    public abstract void BottomQueueAccepted(JNTQueue queue);
    public abstract void BottomQueueRejected(JNTQueue queue);
    public abstract void BottomQueueService(JNTQueue queue);
}
