package org.orb.jnt.JNTTimer;

import java.io.*;
import java.util.Timer;
import java.util.TimerTask;
import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTError.*;


class JNTTask extends TimerTask 
{
  public JNTTask(JNTTimer timerlink) { this.timerlink = timerlink; }

  public JNTTimer timerlink;

  public void run() 
  {
    try
    {
      JNTModuleManager.Signal(timerlink.myModule, JNTModuleOperations.TIMER_EXPIRED, timerlink);
    }
    catch (Exception e) {}

    if (JNTTimerType.ONESHOT == timerlink.GetType())
       timerlink.StopTimer();
  }
}

public class JNTTimer
{
  // debugger
  static Logger deb = Logger.getLogger(JNTTimer.class.getName());

  // constructor
  public JNTTimer(int timeout, int type, String id) 
  {
    this.myModule = JNTModuleManager.currentmodule;
    this.timeout = timeout;
    this.type = type;
    this.id = id;
  }

  // constructor
  public JNTTimer(int timeout, int type)
  {
    this.myModule = JNTModuleManager.currentmodule;
    this.timeout = timeout;
    this.type = type;
    this.id = "";
  }

  // private members
  private Timer timer;
  private int timeout;
  private int type;
  private String id;
  public JNTModule myModule;

  // JNTTimer API
  public void SetTimeout(int timeout, int type) 
  {
    deb.debug("SetTimeout: " + timeout + " type: " + type);
    this.timeout = timeout;
    this.type = type;
  }

  public int GetTimeout()
  {
    return this.timeout;
  }

  public int GetType() 
  { 
    return this.type; 
  }
  
  public String GetID() 
  { 
    return this.id; 
  }

  public void StartTimer() 
  {
    deb.debug("StartTimer");

    if (0 == this.timeout)
    {
      deb.warn("Timeout = 0");
      throw new JNTFailException("JNTTimer::StartTimer");
    }

    timer = new Timer();

    if (JNTTimerType.REPETITIVE == type)
    {
      deb.debug("Start repetitive timer");
      timer.schedule(new JNTTask(this), timeout*1000, timeout*1000);
    }
    else
    {
      deb.debug("Start oneshot timer");
      timer.schedule(new JNTTask(this), timeout*1000);
    }
  }

  public void StopTimer() 
  {
    timer.cancel();
  }
}
