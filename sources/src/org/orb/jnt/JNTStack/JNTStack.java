package org.orb.jnt.JNTStack;


import java.util.Timer;
import java.util.TimerTask;
import java.util.Properties;

import java.io.FileInputStream;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.BasicConfigurator;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTSDU.*;

class JNTTask extends TimerTask 
{
  public JNTTask(JNTStack stack) { this.myStack = stack; }

  // We must keep a stack reference, otherwise the garbagge collector 
  // won't find a reference to this object and clean it invoking
  // the finalize member function.
  private JNTStack myStack;

  public void run() 
  {
    JNTModuleManager.ProcessMessages();
  }

}

public abstract class JNTStack extends Thread
{
  // attributes
  // ----------------------------------------

  // JNT Heartbeat
  private Timer heartbeat;

  // JNT timeout in ms
  private static int timeout = 5; 
  
  // debugger
  static Logger deb = Logger.getLogger(JNTStack.class.getName());

  // member functions
  // ----------------------------------------

  public JNTStack(int priority) 
  {  	  	              	
    int newPriority;
    newPriority = Math.min(priority, MAX_PRIORITY);
    newPriority = Math.max(newPriority, MIN_PRIORITY);

    initialize(newPriority);
  }

  public JNTStack()
  {
    initialize(NORM_PRIORITY);
  }

  public void initialize(int priority)
  {
  	BasicConfigurator.resetConfiguration(); 
    PropertyConfigurator.configureAndWatch("log4j.properties", delay); 
    
    setPriority(priority);
    
    heartbeat = new Timer();
  }

  public void Start()
  throws JNTException
  {
    deb.debug("Start");   

    try
    {
      // initialize ModuleManager
      JNTModuleManager.Initialize(); 
  
      // initialize queue manager
      JNTQueueManager.Initialize();

      // initialize sdu manager
      JNTSDUManager.Initialize();

      // start the thread
      start();
    }
    catch (Exception e) 
    { 
      deb.fatal("Error starting stack");
      throw new JNTFailException("JNTStack::Start");
    }
  }

  public void run()
  {
    JNTMain();

    // start heartbeat once all modules have been created, registered and initialized
    heartbeat.schedule(new JNTTask(this), timeout, timeout);
  }

  public abstract void JNTMain(); 

  //public void Stop()
  public void finalize()
  {
    deb.debug("Finalize");

    // finalize the module manager
    JNTModuleManager.Finalize();

    // finalize the queue manager
    JNTQueueManager.Finalize();

    // finalize sdu manager
    JNTSDUManager.Finalize();

    // get myself
    Thread myself = Thread.currentThread();
  
    // destroy myself
    myself.destroy();
  }
  
  // set by default 2 minutes
  private long delay = 120000;
}
