package org.orb.jnt.JNTAdapter;

import java.io.*;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.apache.log4j.xml.DOMConfigurator;

public abstract class JNTSocketConnectionInterface 
{
  public JNTSocketConnectionInterface()
  {
  }
  
	
	static protected transient Logger log = Logger.getLogger(JNTSocketConnectionInterface.class);
	
	/**
	 * Start. Starts a server socket
	 *
	 * @throws IOException in the socket can't create
	 */
	public void start() throws IOException
	{
		this.isStopping = false;
		
		// create thread to listen in the socket
		this.rxThread = new ProcessThread();
		this.rxThread.setPriority(Thread.MIN_PRIORITY);
		this.rxThread.start();
	} // end start()
	
	/**
	 * Stop. Stops the server socket
	 */
	public void stop()
	{
		this.isStopping = true;
		this.closeConnection(true);

		//this.onSocketFailure();
		
		try
		{
			this.rxThread.join();
		}//end try
		catch(InterruptedException ex)
		{
		}//end catch
	}
	
	public void executeProcess() 
		throws Exception
	{
		read();
	}
	
	protected void closeConnection(boolean all)
	{
		try
		{
			if (clientSocket != null && !clientSocket.isClosed())
			{
				this.clientSocket.close();
			}//end if
		}
		catch (IOException ex)
		{
		}
	}//end closeConnection
	
	/**
	 * Thread to process the listenning in the socket
	 */
	protected class ProcessThread extends Thread
	{
		public void run()
		{
			while (!isStopping)
			{
				log.debug("ProcessThread (" + name + ") :: retryConnection ... ");
				if(retryConnection())
				{					
					try
					{
						while(!isStopping)
						{
							// read a command
							try
							{
								executeProcess();
							}
							//RxSocket Timeout - continue
							catch (InterruptedIOException ex) 
							{
								log.debug("ProcessThread (" + name + ") :: executeProcess InterruptedIOException (" + clientSocket.getLocalSocketAddress().toString()+ "): " + ex.getMessage());	
							}
						}//end while
					}//end try
					catch (Exception ex)
					{            
						log.error("ProcessThread (" + name + ") :: Exception (" + clientSocket.getLocalSocketAddress().toString()+ "): " + ex.getMessage());
						closeConnection(false);
						onSocketFailure();
					}//end catch
				}//end if(retryConnection)

				try
				{
					sleep(TIME_TRY_CONNECT);
				} //end try
				catch (InterruptedException ex1)
				{} //end cath
			} // end while(!isStopping)
		} // end run
	} // end ProcessThread
	
	/**
	 * Read messages from the socket stream
	 * 
	 * @throws IOException
	 * @throws COGAException 
	 */
	protected void read() throws Exception, IOException
	{
		InputStream stream = this.clientSocket.getInputStream();
		int bufferLength = -1;
		// Read byte coming from the client
		if ( (bufferLength = stream.read(this.buffer, 0, BUFFER_SIZE)) >= 0 )
		{
			log.debug("readMessage (" + name + ") :: read {" + bufferLength + ",0}");
			this.msgBuffer = new byte[bufferLength];      
      System.arraycopy(this.buffer, 0, this.msgBuffer, 0, bufferLength);
      
			processMessage(msgBuffer);				      
		}//end if(read > 0)
		else
		{
      log.error("End of the socket stream is rearched");
			throw new IOException("End of the socket stream is rearched");
		}//end else
	}
	
	/**
	 * Sends a messsage through socket
	 * 
	 * @param message Message to be sent
	 */
	public void write(byte[] message)
	{	try
		{	
			if(message != null && message.length > 0)
			{
				OutputStream stream = this.clientSocket.getOutputStream();
				//stream.write(Utils.getBytes(message.length)); 	// Size of the message
				stream.write(message);     						// Message
				log.debug("writeMessage (" + name + ") :: message (" + message.length + ") " /*+ PrintBits.getHex(Utils.getBytes(message.length)) + " " + PrintBits.getHex(message)*/);
			}//end if
		}//end try
	    catch(IOException iex)
	    {
            log.debug("writeMessage (" + name + ") :: IOException : " + iex.getMessage());
            closeConnection(false);
	    }//end catch
	}//end writeMessage
	
	/**
	 * When the socket connection fail retries connection
	 */
	/**
	 * when the socket connection fail, try to connect again.
	 */
	protected boolean retryConnection()
	{
		if(this.connectSocket())
		{
			//Init buffers
			this.bufferIndex = 0;
			this.msgBuffer = null;
			this.msgBufferIndex = 0;

			this.onSocketConnection();
			return true;
		}//end if
		else
		{
			this.closeConnection(false);
			this.onSocketFailure();
			return false;
		}//end else
	} // end retryConnection()
	
	/**
	 * connect to a remote socket
	 *
	 * @return if the could connect with the remote socket
	 */
	protected abstract boolean connectSocket();
	
	/**
	 * Processes one message when arrive at the server
	 *
	 * @param message Message to be processed
	 */
	protected abstract void processMessage(byte[] message) throws Exception;
	
	/**
	 * When one client is connected
	 */
	protected abstract void onSocketConnection();
	
	/**
	 * When the socket connection fail
	 *
	 */
	protected abstract void onSocketFailure();
	
	/**
	 * Name
	 */
	protected String name = null;
	
	/**
	 * time out for the sendCommand
	 */
	protected int timeout = CONNECT_TIMEOUT;
		
	/**
	 * if the monitoring is closing
	 */
	protected boolean isStopping = false;
		
	/**
	 * Communication Socket
	 */
	protected Socket clientSocket;
	
	/**
	 * Reception thread
	 */
	private Thread rxThread = null;
	
	/**
	 * Temporal memory, default value : 10KB
	 */
	protected int BUFFER_SIZE = 10000;	
	
	/**
	 * Internal buffer used to storage the stream of bytes recived 
	 * from the socket channel
	 */
	private byte buffer[] = new byte[BUFFER_SIZE];
	private int bufferIndex = 0;
	
	private byte msgBuffer[] = null;
	private int msgBufferIndex = 0;
	
	/**
	 * Property identifiers
	 */
	protected static final int CONNECT_TIMEOUT = 10000;
	
	/**
	 * time waiting before try connect again
	 */
	protected static final long TIME_TRY_CONNECT = 1000;
}