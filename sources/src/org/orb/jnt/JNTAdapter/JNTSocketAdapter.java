package org.orb.jnt.JNTAdapter;

import java.io.IOException;
import java.io.InterruptedIOException;

import java.lang.*;

import java.net.InetSocketAddress;
import java.net.ServerSocket;
import java.net.Socket;

import org.apache.log4j.Logger;
import org.orb.jnt.JNTError.JNTException;
import org.orb.jnt.JNTError.JNTFailException;
import org.orb.jnt.JNTError.JNTRuntimeException;
import org.orb.jnt.JNTModule.JNTModule;
import org.orb.jnt.JNTModule.JNTModuleManager;
import org.orb.jnt.JNTModule.JNTModuleOperations;
import org.orb.jnt.JNTQueue.*;

public abstract class JNTSocketAdapter extends JNTAdapter
{
  static Logger deb = Logger.getLogger(JNTSocketAdapter.class.getName());
  
  // attributes
  // ------------------------------------------------------  
  JNTClientSocket clientsocket;
  JNTServerSocket serversocket;
  
  // member functions 
  // ------------------------------------------------------

  public JNTSocketAdapter() 
  {    
  }
  
  public void Write(byte[] message)
  {
    serversocket.write(message);
  }
  
  public void OpenConnection(String host, int port)
  throws JNTException
  {
    try
    {
      clientsocket = new JNTClientSocket(host, port);   
      clientsocket.start();
    }
    catch (IOException e)
    {
      throw new JNTFailException("JNTSocketAdapter::OpenConnection", e.getMessage());
    }
  }
  
  public void AcceptConnection(int port)
  throws JNTException
  {
    try
    {
      serversocket = new JNTServerSocket(port);   
      serversocket.start();
    }
    catch (IOException e)
    {
      deb.fatal(e.getMessage());
      throw new JNTFailException("JNTSocketAdapter::OpenConnection", e.getMessage());
    }
  }
  
  public void CloseClientConnection()
  {
    if (null != clientsocket)
    {
      clientsocket.stop();
      ClientConnectionClosed();
    }
  }
  
  public void InnerConnectionService(byte[] message)
  {
    // Put the messaeg in the queue
      try
      {
        JNTModuleManager.Signal(this, JNTModuleOperations.CONNECTION_SERVICE, message);               
      }
      catch (JNTRuntimeException re) { throw re; }
      catch (Exception e)
      {
        e.printStackTrace();
      }
  }
      
  public class JNTClientSocket extends JNTSocketConnectionInterface
  {
    public JNTClientSocket(String host, int port)
    {
      this.txHost = host;
      this.txPort = port;
    }
  
    /**
     * Host where the server will connect
     */
    protected String txHost;
    protected int MAIN_HOST = 0;
    protected int BACKUP_HOST = 1;
    
    /**
     * Port in the host where the server will connect
     */
    protected int txPort = -1;
    
    /**
     * Connection retries
     */
    protected int maxNumberOfTry = DEFAULT_TX_RETRIES;
      
    /**
     * Property identifiers
     */
    protected static final int DEFAULT_TX_RETRIES = 1;
    
    /**
     * connect to a remote socket
     *
     * @return if the could connect with the remote socket
     */
    protected boolean connectSocket()
    {
      // connect with the remote socket
      this.clientSocket = null;
      
      if(txHost != null)
      {
        log.debug("connectSocket (" + name + ") :: intentando conexion (" + this.timeout + ")... "+txHost);
        for (int i = 0; i < this.maxNumberOfTry; i++)
        {
          try
          {
            // Open the socket
            this.clientSocket = new Socket();
            this.clientSocket.connect(new InetSocketAddress(txHost, txPort), CONNECT_TIMEOUT);
            this.clientSocket.setSoTimeout(this.timeout);
            this.clientSocket.setSoLinger(true, 0);
            
            log.info("connectSocket (" + name + ") :: conexion establecida ... " +  txHost + " OK");
            return true;
          } //end try
          catch (Exception ex)
          {
            log.debug("connectSocket (" + name + ") :: conexion establecida ... " +  txHost + " NOK " + ex.getMessage());
          } //end catch
          
          synchronized (this)
          {
            try
            {
              wait(TIME_TRY_CONNECT);
            } //end try
            catch (InterruptedException ex1)
            {} //end cath
          } //end synchronized
        } // end for()
      }//end if
      
      return false;
    } // end connectSocket()
    
    protected void onSocketFailure()
    {
      ClientConnectionClosed();
    }      
    
    protected void onSocketConnection()
    {
      ClientConnectionOpened();
    }
    
    /**
     * A message is ready to be sent, create an SDU and send it up
     * through the top queue if exists.
     * @throws java.lang.Exception
     * @param message
     */
    protected void processMessage(byte[] message) 
    throws Exception
    {
       ConnectionService(message);
    }    
  } 
  
  public class JNTServerSocket  extends JNTSocketConnectionInterface
  {
    public JNTServerSocket(int port)
    {      
      this.rxPort = port;
    }
    
    /**
     * Port where the client will connect in the server
     */
    protected int rxPort = -1;
    
    /**
     * Socket in the server
     */
    private ServerSocket serverSocket;
    
    /**
     * Property identifiers
     */
    private static final int ACCEPT_TIMEOUT = 4000;
    
    /**
     * Start. Starts a server socket
     * 
     * @throws IOException in the socket can't create
     */
    public void start() throws IOException
    {
      if(this.rxPort > 0)
      {
        // Create the server socket
        this.serverSocket = new ServerSocket(this.rxPort);
        this.serverSocket.setSoTimeout(ACCEPT_TIMEOUT);
      }//end if
      
      super.start();
    } // end start()
    
    /**
     * Accept one connection in rx (server socket)
     *
     * @return if some client is connected
     */
    protected boolean connectSocket()
    {
      this.clientSocket = null;
      log.debug("connectSocket (" + name + ") :: aceptando conexion ... ");
      try
      {
        // Accept conections
        try
        {
          this.clientSocket = this.serverSocket.accept();
        }//end try
        catch (InterruptedIOException ex1)
        {
        }//end catch
        
        if (this.clientSocket != null)
        {
          this.clientSocket.setSoTimeout(this.timeout);
          this.clientSocket.setSoLinger(true,0);
          
          log.debug("connectSocket (" + name + ") :: conexion aceptada OK");
          return true;
        }//end if
        else
        {
          log.debug("connectSocket (" + name + ") :: conexion aceptada NOK");
          return false;
        }//end else
      }//end try
      catch (IOException ex)
      {
        log.debug("connectSocket (" + name + ") :: conexion aceptada NOK " + ex.getMessage());
        return false;
      }//end catch
    } // end connectSocket()
    
    protected void closeConnection(boolean all)
    {
      try
      {
        if(all && serverSocket != null && !serverSocket.isClosed())
        {
          this.serverSocket.close();
        }//end if
        
        super.closeConnection(all);
      }
      catch (IOException ex)
      {
        log.info("closeConnection (" + name + ") :: Error closing server socket: " + ex.getMessage());
      }
    }//end closeConnection	
    
    protected void onSocketFailure()
    {
      ServerConnectionRejected();
    }      
    
    protected void onSocketConnection()
    {
      ServerConnectionAccepted();
    }
    
    /**
     * A message is ready to be sent, create an SDU and send it up
     * through the top queue if exists.
     * @throws java.lang.Exception
     * @param message
     */
    protected void processMessage(byte[] message)     
    {
      try
      {
        InnerConnectionService(message);      
      }
      catch (Exception e)
      {
        log.fatal("Could not send message up: " + e.getMessage());
      }
    }  
  }
}
