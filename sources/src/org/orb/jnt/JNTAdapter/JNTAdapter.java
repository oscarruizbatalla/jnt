package org.orb.jnt.JNTAdapter;
import org.orb.jnt.JNTModule.JNTModule;
import org.orb.jnt.JNTQueue.JNTQueue;

public abstract class JNTAdapter extends JNTModule
{
  public JNTAdapter()
  {
  }
  
  // Bottom queue - adapters have no bottom queue
  public void BottomQueueOpened(JNTQueue queue) {}
  public void BottomQueueClosed(JNTQueue queue) {}
  public void BottomQueueAccepted(JNTQueue queue) {}
  public void BottomQueueRejected(JNTQueue queue) {}
  public void BottomQueueService(JNTQueue queue) {}
    
  public abstract void ClientConnectionOpened();
  public abstract void ClientConnectionClosed();
  public abstract void ServerConnectionClosed();
  public abstract void ServerConnectionAccepted();
  public abstract void ServerConnectionRejected();
  
  public abstract void ConnectionService(byte[] message);
}