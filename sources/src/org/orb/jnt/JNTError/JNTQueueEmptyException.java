package org.orb.jnt.JNTError;


public class JNTQueueEmptyException extends JNTRuntimeException
{
  public JNTQueueEmptyException(String location, String cause) 
  { 
    super(location, cause); 
  }
  
  public JNTQueueEmptyException(String location)
  {
    super(location);
  }

  public String GetError() 
  { 
    return new String(location + ":Queue empty - [" + cause + "]");
  } 
}

