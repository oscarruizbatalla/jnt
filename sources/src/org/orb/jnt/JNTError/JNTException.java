package org.orb.jnt.JNTError;

// inherit from Exception makes it mandatory
// to catch the generated exceptions

public abstract class JNTException extends Exception
{
  // where exception has been raised
  public String location;
  public String cause;

  public JNTException(String location, String cause)
  {
    this.location = location;
    this.cause = cause;
  }

  public JNTException(String location)
  {
    this.location = location;
    this.cause = "";
  }

  public abstract String GetError(); 
}
