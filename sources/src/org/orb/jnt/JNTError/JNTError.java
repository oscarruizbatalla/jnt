package org.orb.jnt.JNTError;

public class JNTError
{
  // JNT Errors
  // ----------------------------------
  
  // Success
  public static final int SUCCESS = 0;

  // Fail 
  public static final int FAIL = 1;


  // get string error
  public static String toString(int error)
  {
    String out;

    switch (error)
    {
      case JNTError.SUCCESS:
        out = "Success";
        break;
      case JNTError.FAIL:
        out = "Fail";
        break;
      default:
        out = "Unknown error";
        break;
    }
  
    return out;
  }
}
