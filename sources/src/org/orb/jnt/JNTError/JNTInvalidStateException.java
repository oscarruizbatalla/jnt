package org.orb.jnt.JNTError;


public class JNTInvalidStateException extends JNTRuntimeException
{
  public JNTInvalidStateException(String location, String cause) 
  { 
    super(location, cause); 
  }
  
  public JNTInvalidStateException(String location)
  {
    super(location);
  }

  public String GetError() 
  { 
    return new String(location + ":Invalid State - [" + cause + "]");
  } 
}

