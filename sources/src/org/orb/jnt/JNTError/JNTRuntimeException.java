package org.orb.jnt.JNTError;

// inherit from RuntimeException makes it not mandatory
// to catch the generated exceptions

public abstract class JNTRuntimeException extends RuntimeException
{
  // where exception has been raised
  public String location;
  public String cause;

  public JNTRuntimeException(String location, String cause)
  {
    this.location = location;
    this.cause = cause;
  }

  public JNTRuntimeException(String location)
  {
    this.location = location;
    this.cause = "";
  }

  public abstract String GetError(); 
}
