package org.orb.jnt.JNTError;


public class JNTNoMemoryException extends JNTException
{
  public JNTNoMemoryException(String location, String cause) 
  { 
    super(location, cause); 
  }

  public JNTNoMemoryException(String location)
  {
    super(location);
  }

  public String GetError() 
  { 
    return new String(location + ":No memory available - [" + cause + "]");
  }
}

