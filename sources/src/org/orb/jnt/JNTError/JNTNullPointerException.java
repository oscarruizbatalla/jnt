package org.orb.jnt.JNTError;


public class JNTNullPointerException extends JNTException
{
  public JNTNullPointerException(String location, String cause) 
  { 
    super(location, cause); 
  }

  public JNTNullPointerException(String location)
  {
    super(location);
  }

  public String GetError() 
  { 
    return new String(location + ":Null pointer - [" + cause + "]"); 
  }
}

