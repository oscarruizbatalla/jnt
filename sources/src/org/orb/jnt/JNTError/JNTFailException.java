package org.orb.jnt.JNTError;


public class JNTFailException extends JNTRuntimeException
{
  public JNTFailException(String location, String cause) 
  { 
    super(location, cause); 
  }

  public JNTFailException(String location)
  {
    super(location);
  }

  public String GetError() 
  { 
    return new String(location + ":Operation failed - [" + cause + "]");
  }
}

