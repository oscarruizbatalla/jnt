package org.orb.jnt.JNTQueue;

import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;

public class JNTQueue
{
  // member functions
  // -----------------------------------

  // debugger
  static Logger deb = Logger.getLogger(JNTQueue.class.getName());

  // constructor
  public JNTQueue() {}

  // Close a queue
  public void Close()
  {
    deb.debug("Close queue");

    try
    {
      JNTQueueManager.Close(JNTModuleManager.currentmodule, this);
    }
    catch (JNTException e) { throw new JNTFailException(e.GetError()); }
    catch (JNTRuntimeException re) { throw re; }
  }

  // Accept a queue connection
  public void Accept()
  {
    deb.debug("Accept queue");

    try
    {
      JNTQueueManager.Accept(JNTModuleManager.currentmodule, this);
    }
    catch (JNTException e) { throw new JNTFailException(e.GetError()); }
    catch (JNTRuntimeException re) { throw re; }
  }

  // Reject a queue connection
  public void Reject()
  {
    deb.debug("Reject queue");

    try
    {
      JNTQueueManager.Reject(JNTModuleManager.currentmodule, this);
    }
    catch (JNTException e) { throw new JNTFailException(e.GetError()); }
    catch (JNTRuntimeException re) { throw re; }
  }

  // send messages throught queue
  public void Send(JNTSDU sdu) 
  throws JNTException 
  {
    deb.debug("Send to queue");

    try
    {
      JNTQueueManager.Send(JNTModuleManager.currentmodule, this, sdu);
    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }    
  }

  public JNTSDU Receive()
  throws JNTException
  {
    deb.debug("Receive from queue");

    try
    {
      return JNTQueueManager.Receive(JNTModuleManager.currentmodule, this);
    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
  }
}
