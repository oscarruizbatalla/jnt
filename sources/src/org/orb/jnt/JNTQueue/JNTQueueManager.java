package org.orb.jnt.JNTQueue;

import java.util.*;
import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;

public class JNTQueueManager
{
  // attributes
  // -----------------------------------
  public static LinkedList queueList;
  
  // debugger
  static Logger deb = Logger.getLogger(JNTQueueManager.class.getName());

  // member functions
  // -----------------------------------

  // constructor
  public JNTQueueManager() {}

  // initializer
  public static void Initialize()
  {
    deb.debug("Initialize");

    queueList = new LinkedList(); 
  }
  
  public static void Finalize()
  {
    deb.debug("Finalize");

    if (0 == queueList.size())
    {
      deb.warn("Empty queueList");
      return;
    }

    ListIterator iterator = queueList.listIterator();

    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      try
      {
        queueMeta.Close(JNTModuleManager.currentmodule);
      }
      catch (Exception e) 
      {
        deb.fatal("Error finalizing");
      }
    }
  
    queueList.clear();
  }

  // Open a queue
  public static void Open(JNTModule topmodule, JNTModule bottommodule) 
  throws JNTException
  { 
    deb.debug("Open queue");

    if (null == queueList)
    {
      deb.warn("Empty queueList"); 
      throw new JNTNullPointerException("JNTQueueManager::Open");
    }

    if (!JNTModuleManager.IsValid(topmodule) || !JNTModuleManager.IsValid(bottommodule))
    {
      // modules not registered
      deb.warn("Modules not registered");
      throw new JNTFailException("JNTQueueManager::Open");
    }
   
    JNTQueueMeta queue = new JNTQueueMeta(topmodule, bottommodule);
 
    try
    {
        synchronized(queueList) 
        {
            queueList.addLast(queue);
            queue.Open(JNTModuleManager.currentmodule);
            queue.notify();
        }      
    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
    catch (Exception e) { throw new JNTFailException("JNTQueueManager::Open"); }
  }

  public static void Close(JNTModule module, JNTQueue queue)
  throws JNTException
  {
    deb.debug("Close queue");

    if (null == queueList)
    {
      deb.warn("Empty queueList");
      throw new JNTNullPointerException("JNTQueueManager::Close");
    }

    ListIterator iterator = queueList.listIterator();
  
    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      if (queueMeta.GetQueue() == queue)
      {
         try
         { 
             synchronized(queueList) 
             {
                 queueList.addLast(queue);
                 queueMeta.Close(module);
                 queueList.remove(queueMeta);
             }                        
             break;
         }
         catch (JNTRuntimeException re) { throw re; }
         catch (JNTException e) { throw e; }
         catch (Exception e) { throw new JNTFailException("JNTQueueManager::Close"); }
      }
    }
  }

  public static void Accept(JNTModule module, JNTQueue queue)
  throws JNTException
  {
    deb.debug("Accept queue");
 
    if (null == queueList)
    {
      deb.warn("Empty queueList");
      throw new JNTNullPointerException("JNTQueueManager::Close");
    }

    ListIterator iterator = queueList.listIterator();

    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      if (queueMeta.GetQueue() == queue)
      {
         try
         {
           queueMeta.Accept(module);
           break;
         }
         catch (JNTRuntimeException re) { throw re; }
         catch (JNTException e) { throw e; }
      }
    }
  }

  public static void Reject(JNTModule module, JNTQueue queue)
  throws JNTException 
  {
    deb.debug("Reject queue");

    if (null == queueList)
    {
      deb.warn("Empty queueList");
      throw new JNTNullPointerException("JNTQueueManager::Close");
    }

    ListIterator iterator = queueList.listIterator();

    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      if (queueMeta.GetQueue() == queue)
      {
         try 
         { 
           queueMeta.Reject(module); 
           break;
         }
         catch (JNTException e) { throw e; }
         catch (JNTRuntimeException re) { throw re; }
      }
    }
  }

  public static void Send(JNTModule module, JNTQueue queue, JNTSDU sdu)
  throws JNTException
  {
    deb.debug("Send to queue");

    if (null == queueList)
    {
      deb.warn("Empty queueList");
      throw new JNTNullPointerException("JNTQueueManager::Send");
    }

    ListIterator iterator = queueList.listIterator();

    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      if (queueMeta.GetQueue() == queue)
      {
         try
         {
           queueMeta.Send(module, sdu);
           break;
         }
         catch (JNTException e) { throw e; }
         catch (JNTRuntimeException re) { throw re; }
      }
    }
  }


  public static JNTSDU Receive(JNTModule module, JNTQueue queue)
  throws JNTException
  {
    deb.debug("Receive from queue");

    if (null == queueList)
    {
      deb.warn("Empty queueList");
      throw new JNTNullPointerException("JNTQueueManager::Receive");
    }

    ListIterator iterator = queueList.listIterator();

    while (iterator.hasNext())
    {
      JNTQueueMeta queueMeta = (JNTQueueMeta)iterator.next();

      if (queueMeta.GetQueue() == queue)
      {
         try
         {
           return queueMeta.Receive(module);
         }
         catch (JNTException e) { throw e; }
         catch (JNTRuntimeException re) { throw re; }
      }
    }

    return null;
  }


}
