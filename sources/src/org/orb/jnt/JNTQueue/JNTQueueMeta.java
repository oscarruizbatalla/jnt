package org.orb.jnt.JNTQueue;

import java.util.*;
import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;


public class JNTQueueMeta
{
  // attributes
  // -----------------------------------
  
  // top and bottom modules array
  public JNTModule module[];
 
  // my queue
  public JNTQueue  queue;

  // queue state
  public int       state;

  // transitions table [state][operation]
  public int transTable[][];

  // sdu containers
  public LinkedList sduContainer[];

  public static final int MODULE_TOP = 0;
  public static final int MODULE_BOTTOM = 1;
  public static final int MODULE_INVALID = -1;

  // queue operations
  public static final int OPEN = 0;

  public static final int TOP_ACCEPT = 1;
  public static final int TOP_REJECT = 2;

  public static final int BOTTOM_ACCEPT = 3;
  public static final int BOTTOM_REJECT = 4;

  public static final int CLOSE = 5;

  public static final int OPERATIONS_NUM = 6;

  // queue states
  public static final int INVALID = 0;

  public static final int IDLE = 1;
  public static final int NEW = 2;

  public static final int TOP_ACCEPTED = 3;
  public static final int BOTTOM_ACCEPTED = 4;
  
  public static final int ACTIVE = 5;

  public static final int TOP_REJECTED = 6;
  public static final int BOTTOM_REJECTED = 7;
  
  public static final int REJECTED = 8;

  public static final int CLOSED = 9;

  public static final int STATES_NUM = 10;

  // debugger
  static Logger deb = Logger.getLogger(JNTQueueMeta.class.getName());

  // member functions
  // -----------------------------------

  // constructor
  public JNTQueueMeta(JNTModule top, JNTModule bottom) 
  {
    deb.debug("Constructor");

    module = new JNTModule[2];
    sduContainer = new LinkedList[2];

    queue = new JNTQueue();

    state = IDLE;

    transTable = new int[STATES_NUM][OPERATIONS_NUM];

    FillTransTable();

    module[MODULE_TOP] = top;
    module[MODULE_BOTTOM] = bottom;

   sduContainer[MODULE_TOP] = new LinkedList();
   sduContainer[MODULE_BOTTOM] = new LinkedList();
  }

  // Close a queue
  public void Open(JNTModule module)
  throws JNTException
  {
    deb.debug("Open queue");

    try
    {
      // change state
      Transition(OPEN);
     
      // notify top module
      JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_OPENED, this.queue);

      // notify bottom module
      JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_OPENED, this.queue);
    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
  }

  // Close a queue
  public void Close(JNTModule module)
  throws JNTException
  {
    deb.debug("Close queue");

    try
    {
      // change state
      Transition(CLOSE);

      // notify top module
      JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_CLOSED, this.queue);
     
      // notify bottom module
      JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_CLOSED, this.queue);
    }
    catch (JNTException e) { throw e;}
    catch (JNTRuntimeException re) { throw re; }
  }

  // Accept a queue connection
  public void Accept(JNTModule module)
  throws JNTException
  {
    deb.debug("Accept queue");

    try 
    {
      int side = GetSide(module);

      int operation = (side == MODULE_TOP? TOP_ACCEPT : BOTTOM_ACCEPT);

      // change state
      Transition(operation);
 
      switch (state)
      {
        case ACTIVE:
          // notify top module
          JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_ACCEPTED, this.queue);
          // notify bottom module
          JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_ACCEPTED, this.queue);
          break;
        case REJECTED:
          // notify top module
          JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_REJECTED, this.queue);
          // notify bottom module
          JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_REJECTED, this.queue);
          break;
        case TOP_ACCEPTED:
        case BOTTOM_ACCEPTED:
        case TOP_REJECTED:
        case BOTTOM_REJECTED:
          break;
        default:
          throw new JNTInvalidStateException("JNTQueueMeta::Accept","Invalid operation/state" + state);
      }
    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
  }

  // Reject a queue connection
  public void Reject(JNTModule module)
  throws JNTException
  {
    deb.debug("Reject queue");

    try
    {
      int side = GetSide(module);

      int operation = (side == MODULE_TOP? TOP_REJECT : BOTTOM_REJECT);

      // change state
      Transition(operation);

      switch (state)
      {
        case REJECTED:
          // notify top module
          JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_REJECTED, this.queue);
          // notify bottom module
          JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_REJECTED, this.queue);
          break;
        default:
          break;
      }

    }
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
  }

  // send messages throught queue
  public void Send(JNTModule module, JNTSDU sdu) 
  throws JNTException
  {
    deb.debug("Send to queue");

    if (ACTIVE != state)
    {
      deb.fatal("Invalid state");
      throw new JNTInvalidStateException("JNTQueueMeta::Send","Invalid queue state");
    }
    
    
    try
    {
        int side = GetSide(module);

        if (null == sduContainer[side])
        {
        deb.fatal("Container null");
        throw new JNTNullPointerException("JNTQueueMeta::Send","Null sduContainer");
        }
      
        synchronized(sduContainer)
        {
          // put sdu into container 
          sduContainer[side].addLast(sdu);
        
          switch (side)
          {
            case MODULE_TOP:
              // notify bottom module
              JNTModuleManager.Signal(this.module[MODULE_BOTTOM], JNTModuleOperations.TOP_QUEUE_SERVICE, this.queue);
              break;
            case MODULE_BOTTOM:
              // notify top module
              JNTModuleManager.Signal(this.module[MODULE_TOP], JNTModuleOperations.BOTTOM_QUEUE_SERVICE, this.queue);
              break;
            default:
              throw new JNTFailException("JNTQueueMeta::Send", "Invalid module side");
          }             
          sduContainer.notify();
      }
    }    
    catch (JNTException e) { throw e; }
    catch (JNTRuntimeException re) { throw re; }
    catch (Exception e)
    {
      e.printStackTrace();
    }
  }

  // receive a message from the queue
  public JNTSDU Receive(JNTModule module)
  throws JNTException
  {
    deb.debug("Receive from queue");

    if (ACTIVE != state) 
    {
      deb.fatal("Invalid state");
      throw new JNTInvalidStateException("JNTQueueMeta::Receive","Invalid queue state");
    }
    
    try
    {
        int side = GetSide(module);
        int opoSide = (side == MODULE_TOP? MODULE_BOTTOM : MODULE_TOP);
      
        synchronized(sduContainer)
        {    
          if (sduContainer[opoSide].size() != 0)
          {
            sduContainer.notify();
            return (JNTSDU) sduContainer[opoSide].removeFirst();
          }
          else
          {
            deb.warn("Container empty");
            sduContainer.notify();
            throw new JNTQueueEmptyException("JNTQueueMeta::Receive","No more messages");
          }          
        }
    }
    catch (JNTRuntimeException re) { throw re; }
    catch (Exception e) { throw new JNTFailException("JNTQueueMeta::Receive",e.getMessage()); }
  }

  public int GetSide(JNTModule module)
  {
     if (this.module[MODULE_TOP] == module)
     {
       return MODULE_TOP;
     } 
     else if (this.module[MODULE_BOTTOM] == module)
     {
       return MODULE_BOTTOM;
     }

     throw new JNTFailException("JNTQueueMeta::GetSide");
  }

  public JNTQueue GetQueue()
  {
    return this.queue;
  }

  public void Transition(int operation)
  {
    if (INVALID == transTable[state][operation])
    {
      deb.fatal("Invalid operation: " + operation);
      throw new JNTInvalidStateException("JNTQueueMeta::Transition", "Invalid operation" + operation);
    }
 
    state = transTable[state][operation];
  }

  private void FillTransTable()
  {
    for (int _state = 0; _state<STATES_NUM; _state++)
    {
      for (int _operation = 0; _operation<OPERATIONS_NUM; _operation++)
      {
	transTable[_state][_operation] = INVALID;
      }
    }

    // write valid transitions
    transTable[IDLE][OPEN] = NEW;

    transTable[NEW][TOP_ACCEPT] = TOP_ACCEPTED;
    transTable[NEW][BOTTOM_ACCEPT] = BOTTOM_ACCEPTED;
    transTable[NEW][TOP_REJECT] = TOP_REJECTED;
    transTable[NEW][BOTTOM_REJECT] = BOTTOM_REJECTED;

    transTable[TOP_ACCEPTED][BOTTOM_ACCEPT] = ACTIVE;
    transTable[TOP_ACCEPTED][BOTTOM_REJECT] = REJECTED;
    
    transTable[BOTTOM_ACCEPTED][TOP_ACCEPT] = ACTIVE;
    transTable[BOTTOM_ACCEPTED][TOP_REJECT] = REJECTED;

    transTable[TOP_REJECTED][BOTTOM_ACCEPT] = REJECTED;
    transTable[TOP_REJECTED][BOTTOM_REJECT] = REJECTED;

    transTable[BOTTOM_REJECTED][TOP_ACCEPT] = REJECTED;
    transTable[BOTTOM_REJECTED][TOP_REJECT] = REJECTED;
  
    transTable[ACTIVE][CLOSE] = CLOSED;
  }

  public int GetState()
  {
    return state;
  }
}
