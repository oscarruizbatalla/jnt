package org.orb.jnt.JNTSDU;

import java.util.*;
import org.apache.log4j.*;

import org.orb.jnt.JNTError.*;

public class JNTSDUManager
{
  // attributes
  // ----------------------------------------
  public static LinkedList sduList;

  // debugger
  static Logger deb = Logger.getLogger(JNTSDUManager.class.getName());

  // member functions
  // ----------------------------------------
 
  // constructor
  public JNTSDUManager() {}

  // sdu dispenser
  public static JNTSDU New()
  throws JNTException
  {
    deb.debug("New SDU");

    // create a new sdu
    JNTSDU sdu = new JNTSDU();
 
    // check for null pointer
    if (null == sdu)
    {
      deb.fatal("No more memory");
      throw new JNTNoMemoryException("JNTSDUManager::NewSDU");
    }
   
    // allocate it into the sdu list 
    sduList.addLast(sdu);

    return sdu;
  }

  public static void Destroy(JNTSDU sdu)
  {
    deb.debug("Destroy");

    if (!sduList.contains(sdu))
    {
      deb.fatal("SDU does not exists");
      throw new JNTFailException("JNTSDUManager::Destroy", "Empty SDU list");
    }
 
    try
    {
      sdu.Destroy();
      sduList.remove(sdu);
    }
    catch (Exception e)
    {
      deb.fatal("SDU not in list");
      throw new JNTFailException("JNTSDUManager::Destroy", "SDU not in list");
    }

  }
 
  // initializer
  public static void Initialize()
  {
    deb.debug("Initialize");

    sduList = new LinkedList();
  }

  // finalizer
  public static void Finalize()
  {
    deb.debug("Finalize");

    sduList.clear();
  }

}
