package org.orb.jnt.JNTSDU;

import java.util.*;
import java.lang.*;
import org.apache.log4j.*;

import org.orb.jnt.JNTError.*;

class JNTCell
{
  // attributes
  // ------------------------------------------------

  public byte data[];
  public int size;

  // member functions
  // ------------------------------------------------

  // constructor
  public JNTCell(byte src[], int size)
  {
    this.size = size;

    data = new byte[size];
  
    for (int i=0; i<size; i++)
      data[i] = src[i];
  }  
}

public class JNTSDU
{
  // attributes
  // ------------------------------------------------

  // list of cells where data is stored
  private LinkedList cellsList;

  // SDU type
  private int type;
  
  // debugger
  static Logger deb = Logger.getLogger(JNTSDU.class.getName());

  // member functions
  // ------------------------------------------------

  // constructor
  protected JNTSDU()
  {
    cellsList = new LinkedList();
  }

  // add data to the tail
  public void AddTail(byte src[], int size)
  throws JNTException
  {
    deb.debug("AddTail");
 
    if (null == cellsList)
    {
      deb.fatal("cellsList empty");
      throw new JNTNullPointerException("JNTSDU::AddTail");
    }

    JNTCell cell = new JNTCell(src, size);
   
    try 
    {
      cellsList.addLast(cell);
    }
    catch (Exception e)
    {
      deb.fatal("Error adding cell to sdu");
      throw new JNTFailException("JNTSDU::AddTail");
    }
  }

  // add data to the head
  public void AddHead(byte src[], int size)
  throws JNTException
  {
    deb.debug("AddHead");

    if (null == cellsList)
    {
      deb.fatal("cellsList empty");
      throw new JNTNullPointerException("JNTSDU::AddTail");
    }

    JNTCell cell = new JNTCell(src, size);

    try
    {
      cellsList.addFirst(cell);
    }
    catch (Exception e)
    {
      deb.fatal("Error adding cell to sdu");
      throw new JNTFailException("JNTSDU::AddTail");
    }
  }

  // removes a given size from the SDU and places it into the destnation buffer
  public int RemoveHead(byte dst[], int size)
  throws JNTException
  {
    deb.debug("RemoveHead");

    if (null == cellsList)
    {
      deb.fatal("cellsList empty");
      throw new JNTNullPointerException("JNTSDU::AddTail");
    }

    int sizeLeft = size;
  
    JNTCell cell;

    while (0 != sizeLeft)
    {
      if (0 != cellsList.size())
      {
        cell = (JNTCell)(cellsList.getFirst());

        int topSize = Math.min(cell.size, sizeLeft); 
        int offset = size - sizeLeft; 

        for (int i=0;i<topSize; i++)
          dst[i + offset] = cell.data[i];

        sizeLeft-=offset;

        if (offset < cell.size)
        {
          Truncate(offset);
          break;
        }
        else
          cellsList.removeFirst();
      }
      else
      {
        break;
      }
    }
  
    return (size - sizeLeft);
  }
 
  // returns queue size in terms of bytes 
  public int GetSize()
  {
    int size = 0;
    int num = cellsList.size();

    while (-1 != --num)
    {
      JNTCell cell = (JNTCell) cellsList.get(num);
      size += cell.size;
    } 

    return size;
  }

  public int GetType()
  {
    return type;
  }

  public void SetType(int type)
  {
    this.type = type;
  }

  protected void Destroy()
  {
    cellsList.clear();
  }

  private void Truncate(int offset)
  {
    JNTCell cell = (JNTCell)(cellsList.removeFirst());
  
    int newSize = cell.size-offset;
    
    byte newData[] = new byte[newSize];

    JNTCell newCell = new JNTCell(newData, newSize);

    cellsList.addFirst(newCell);
  }
}
