package Main;

import org.apache.log4j.*;
import org.orb.jnt.JNTError.JNTException;
import org.orb.jnt.JNTModule.JNTModuleManager;
import org.orb.jnt.JNTStack.JNTStack;

class TestStack extends JNTStack 
{

  // debugger
  static Logger deb = Logger.getLogger(TestStack.class.getName());

  public TestStack(int priority) 
  {
    super(priority);
  }

  public TestStack() {}

  public void JNTMain()
  {
    deb.debug("Starting Stack");

    TCP tcp = new TCP();
    IP ip = new IP();
    
    try 
    {
      JNTModuleManager.Register(tcp, "TCP");
      JNTModuleManager.Register(ip, "IP");
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (Exception e)
    {
      deb.error("System exception");
    }
  }

  public static void main(String args[])
  {
    try
    {
      int priority = Integer.parseInt(args[0]);
      deb.info("Setting priority: " + priority);
      new TestStack(priority).Start();
    }
    catch (JNTException e) { deb.error(e.GetError()); }
    catch (Exception e) 
    {
      try { new TestStack().Start(); }
      catch (JNTException w) { deb.error(w.GetError()); }
    }
  }
}
