package Main;

import org.apache.log4j.*;
import org.orb.jnt.JNTError.JNTException;
import org.orb.jnt.JNTError.JNTNoMemoryException;
import org.orb.jnt.JNTError.JNTQueueEmptyException;
import org.orb.jnt.JNTError.JNTRuntimeException;
import org.orb.jnt.JNTModule.JNTModule;
import org.orb.jnt.JNTModule.JNTModuleManager;
import org.orb.jnt.JNTQueue.JNTQueue;
import org.orb.jnt.JNTQueue.JNTQueueManager;
import org.orb.jnt.JNTSDU.JNTSDU;
import org.orb.jnt.JNTSDU.JNTSDUManager;
import org.orb.jnt.JNTTimer.JNTTimer;
import org.orb.jnt.JNTTimer.JNTTimerType;


class IP extends JNTModule 
{

  public IP() {}

  // one timer
  public JNTTimer dummyTimer;

  // one queue
  public JNTQueue tcpQueue;

  public JNTModule myOID;
  public JNTModule tcp;

  // debugger
  static Logger deb = Logger.getLogger(IP.class.getName());

  // **************************************************
  //
  //  Object management
  //
  // **************************************************

  public void Initialize()
  {
    deb.debug("Initialize");

    tcp = JNTModuleManager.GetModule("TCP");

    myOID = this;
 
    dummyTimer = new JNTTimer(5, JNTTimerType.ONESHOT);
     
    if (null == tcp)
    {
      deb.fatal("Could not find TCP module");
      return;
    }

    // last thing is to open queues, as many methods are called afterwards
    try 
    {
      JNTQueueManager.Open(tcp, myOID);
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
  }

  public void Finalize()
  {
    deb.debug("Finalize");
  }

  // **************************************************
  //
  //  Timers management
  //
  // **************************************************

  public void TimerExpired(JNTTimer timer)
  {
    deb.debug("TimerExpired " + timer.GetTimeout());

    try
    {
      // lets send an sdu to TCP type = 0
      // -------------------------------------------------
      JNTSDU sdu = JNTSDUManager.New(); 

      // sdu data and type
      String sData = "Test string";
      int type = 0;

      byte content[] = sData.getBytes();

      deb.info("Sending sdu type: " + type);
      deb.info("Sending sdu data: " + sData);
 
      sdu.SetType(type);
      sdu.AddTail(content, sData.length());

      tcpQueue.Send(sdu); 
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
  }

  // **************************************************
  //
  //  Top Queue management
  //
  // **************************************************
  
  public void TopQueueOpened(JNTQueue queue)
  {
    deb.debug("TopQueueOpened");

    queue.Accept();
  }

  public void TopQueueClosed(JNTQueue queue)
  {
    deb.debug("TopQueueClosed");
  }

  public void TopQueueAccepted(JNTQueue queue)
  {
    deb.debug("TopQueueAccepted");
 
    tcpQueue = queue;

    try
    {
      dummyTimer.StartTimer();
      deb.info("In 5 seconds, data will start flowing");
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
    catch (Exception e) 
    {
      deb.error("JAVA system exception: " + e.getMessage());
    }
  }

  public void TopQueueRejected(JNTQueue queue)
  {
    deb.debug("TopQueueRejected");
  }

  public void TopQueueService(JNTQueue queue)
  {
    deb.debug("TopQueueService");

    int type = 0;
    int size = 0;
    byte dst[] = null;
    JNTSDU sdu = null;

    try
    {
      // receive phase
      // ---------------------------------------------
      while (true)
      {
        try
        {
          sdu = queue.Receive();
        }
        catch (JNTQueueEmptyException re)
        {
          deb.error(re.GetError());
          break;
        }
        catch (JNTException e) { throw e; }
        catch (JNTRuntimeException re) { throw re; }
       

        size = sdu.GetSize();
        type = sdu.GetType();

        dst = new byte[size];

        sdu.RemoveHead(dst, size);

        deb.info("Receiving sdu type: " + type);
        deb.info("Receiving sdu data: " + new String(dst));

        JNTSDUManager.Destroy(sdu);

        //sleep(1);

        // send it back
        // ---------------------------------------------
        JNTSDU sduBack = JNTSDUManager.New();

        // increment type to keep track of messages sent
        type++;

        deb.info("Sending sdu type: " + type);
        deb.info("Sending sdu data: " + new String(dst));

        sduBack.SetType(type);
        sduBack.AddTail(dst, size);

        tcpQueue.Send(sduBack);

      } // end while
    } // end try
    catch (JNTNoMemoryException e)
    {
      deb.fatal(e.GetError());
      return;
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
  }
  
  // **************************************************
  //
  //  Bottom Queue management
  //
  // **************************************************

  public void BottomQueueOpened(JNTQueue queue)
  {
    deb.debug("BottomQueueOpened");
  }

  public void BottomQueueClosed(JNTQueue queue)
  {
    deb.debug("BottomQueueClosed");
  }

  public void BottomQueueAccepted(JNTQueue queue)
  {
    deb.debug("BottomQueueAccepted");
  }

  public void BottomQueueRejected(JNTQueue queue)
  {
    deb.debug("BottomQueueRejected");
  }

  public void BottomQueueService(JNTQueue queue)
  {
    deb.debug("BottomQueueService");
  }
}

