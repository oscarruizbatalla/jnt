package test;

import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;


class TCP extends JNTModule {

  public TCP() {}

  // one timer
  public JNTQueue ipQueue;
  
  public JNTModule myOID;
  public JNTModule ip;

  // debugger
  static Logger deb = Logger.getLogger(TCP.class.getName());

  // **************************************************
  //
  //  Object management
  //
  // **************************************************

  public void Initialize()
  {
    deb.debug("Initialize");      
  }
  
  public void Finalize()
  {
    deb.debug("Finalize");
  }

  // **************************************************
  //
  //  Timers management
  //
  // **************************************************

  public void TimerExpired(JNTTimer timer)
  {
    deb.debug("TimerExpired " + timer.GetTimeout());
  }

  // **************************************************
  //
  //  Top Queue management
  //
  // **************************************************
 
  public void TopQueueOpened(JNTQueue queue)
  {
    deb.debug("TopQueueOpened");
  }

  public void TopQueueClosed(JNTQueue queue)
  {
    deb.debug("TopQueueClosed");
  }

  public void TopQueueAccepted(JNTQueue queue)
  {
    deb.debug("TopQueueAccepted");
  }

  public void TopQueueRejected(JNTQueue queue)
  {
    deb.debug("TopQueueRejected");
  }

  public void TopQueueService(JNTQueue queue)
  {
    deb.debug("TopQueueService");
  }

  // **************************************************
  //
  //  Bottom Queue management
  //
  // **************************************************

  public void BottomQueueOpened(JNTQueue queue)
  {
    deb.debug("BottomQueueOpened");

    queue.Accept();
  }

  public void BottomQueueClosed(JNTQueue queue)
  {
    deb.debug("BottomQueueClosed");
  }

  public void BottomQueueAccepted(JNTQueue queue)
  {
    deb.debug("BottomQueueAccepted");
 
    ipQueue = queue;
  }

  public void BottomQueueRejected(JNTQueue queue)
  {
    deb.debug("BottomQueueRejected");
  }

  public void BottomQueueService(JNTQueue queue)
  {
    deb.debug("BottomQueueService");

    int type = 0;
    int size = 0;
    byte dst[] = null;
    JNTSDU sdu = null;

    try
    {
      // receive phase
      // ---------------------------------------------
      while (true)
      {
        try
        {
          sdu = queue.Receive();
        }
        catch (JNTQueueEmptyException re)
        {
          deb.warn(re.GetError());
          break;
        }
        catch (JNTException e) { throw e; }
        catch (JNTRuntimeException re) { throw re; }

        size = sdu.GetSize();
        type = sdu.GetType();

        dst = new byte[size];

        sdu.RemoveHead(dst, size);

        deb.info("Receiving sdu type: " + type);
        deb.info("Receiving sdu data: " + new String(dst));

        JNTSDUManager.Destroy(sdu);
      

        // send it back
        // ---------------------------------------------
      
        JNTSDU sduBack = JNTSDUManager.New();

        type++;

        deb.info("Sending sdu type: " + type);
        deb.info("Sending sdu data: " + new String(dst));
      
        sduBack.SetType(type); 
        sduBack.AddTail(dst, size);

        ipQueue.Send(sduBack); 

      } // while end
    } // try end
    catch (JNTNoMemoryException e)
    {
      deb.fatal(e.GetError());
      return;
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
  }
}

