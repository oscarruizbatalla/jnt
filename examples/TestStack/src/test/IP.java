package test;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;

import org.apache.log4j.*;


class IP extends JNTModule 
{

  public IP() {}

  // one timer
  public JNTTimer dummyTimer;

  // one queue
  public JNTQueue tcpQueue;
  public JNTQueue saQueue;

  public JNTModule myOID;
  public JNTModule tcp;

  // debugger
  static Logger deb = Logger.getLogger(IP.class.getName());

  // **************************************************
  //
  //  Object management
  //
  // **************************************************

  public void Initialize()
  {
    deb.debug("Initialize");
  }

  public void Finalize()
  {
    deb.debug("Finalize");
  }

  // **************************************************
  //
  //  Timers management
  //
  // **************************************************

  public void TimerExpired(JNTTimer timer)
  {
    deb.debug("TimerExpired " + timer.GetTimeout());    
  }

  // **************************************************
  //
  //  Top Queue management
  //
  // **************************************************
  
  public void TopQueueOpened(JNTQueue queue)
  {
    deb.debug("TopQueueOpened");
    queue.Accept();
  }

  public void TopQueueClosed(JNTQueue queue)
  {
    deb.debug("TopQueueClosed");
  }

  public void TopQueueAccepted(JNTQueue queue)
  {
    deb.debug("TopQueueAccepted");
 
    // assign queue and accept bottom queue
    tcpQueue = queue; 
    
    saQueue.Accept();
  }

  public void TopQueueRejected(JNTQueue queue)
  {
    deb.debug("TopQueueRejected");
  }

  public void TopQueueService(JNTQueue queue)
  {
    deb.debug("TopQueueService");

    int type = 0;
    int size = 0;
    byte dst[] = null;
    JNTSDU sdu = null;

    try
    {
      // receive phase
      // ---------------------------------------------
      while (true)
      {
        try
        {
          sdu = queue.Receive();
        }
        catch (JNTQueueEmptyException re)
        {
          deb.error(re.GetError());
          break;
        }
        catch (JNTException e) { throw e; }
        catch (JNTRuntimeException re) { throw re; }
       

        size = sdu.GetSize();
        type = sdu.GetType();

        dst = new byte[size];

        sdu.RemoveHead(dst, size);

        deb.info("Receiving sdu type: " + type);
        deb.info("Receiving sdu data: " + new String(dst));

        JNTSDUManager.Destroy(sdu);

        //sleep(1);

        // send it back
        // ---------------------------------------------
        JNTSDU sduBack = JNTSDUManager.New();

        // increment type to keep track of messages sent
        type++;

        deb.info("Sending sdu type: " + type);
        deb.info("Sending sdu data: " + new String(dst));

        sduBack.SetType(type);
        sduBack.AddTail(dst, size);

        saQueue.Send(sduBack);

      } // end while
    } // end try
    catch (JNTNoMemoryException e)
    {
      deb.fatal(e.GetError());
      return;
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
  }
  
  // **************************************************
  //
  //  Bottom Queue management
  //
  // **************************************************

  public void BottomQueueOpened(JNTQueue queue)
  {
    deb.debug("BottomQueueOpened");
    
    saQueue = queue;
    
    tcp = JNTModuleManager.GetModule("TCP");

    myOID = this;
         
    if (null == tcp)
    {
      deb.fatal("Could not find TCP module");
      return;
    }

    // last thing is to open queues, as many methods are called afterwards
    try 
    {
      JNTQueueManager.Open(tcp, myOID);      
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
  }

  public void BottomQueueClosed(JNTQueue queue)
  {
    deb.debug("BottomQueueClosed");
  }

  public void BottomQueueAccepted(JNTQueue queue)
  {
    deb.debug("BottomQueueAccepted");
    
    saQueue = queue;
  }

  public void BottomQueueRejected(JNTQueue queue)
  {
    deb.debug("BottomQueueRejected");
  }

  public void BottomQueueService(JNTQueue queue)
  {
    deb.debug("BottomQueueService");
    
    int type = 0;
    int size = 0;
    byte dst[] = null;
    JNTSDU sdu = null;

    try
    {
      // receive phase
      // ---------------------------------------------
      while (true)
      {
        try
        {
          sdu = queue.Receive();
        }
        catch (JNTQueueEmptyException re)
        {
          deb.error(re.GetError());
          break;
        }
        catch (JNTException e) { throw e; }
        catch (JNTRuntimeException re) { throw re; }
       

        size = sdu.GetSize();
        type = sdu.GetType();

        dst = new byte[size];

        sdu.RemoveHead(dst, size);

        deb.info("Receiving sdu type: " + type);
        deb.info("Receiving sdu data: " + new String(dst));

        JNTSDUManager.Destroy(sdu);

        //sleep(1);

        // send it back
        // ---------------------------------------------
        JNTSDU sduBack = JNTSDUManager.New();

        // increment type to keep track of messages sent
        type++;

        deb.info("Sending sdu type: " + type);
        deb.info("Sending sdu data: " + new String(dst));

        sduBack.SetType(type);
        sduBack.AddTail(dst, size);

        tcpQueue.Send(sduBack);

      } // end while
    } // end try
    catch (JNTNoMemoryException e)
    {
      deb.fatal(e.GetError());
      return;
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
  }
}

