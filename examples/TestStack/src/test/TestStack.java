package test;

import org.apache.log4j.*;

import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTStack.*;
import org.orb.jnt.JNTError.*;

class TestStack extends JNTStack 
{
  // debugger
  static Logger deb = Logger.getLogger(TestStack.class.getName());

  public TestStack(int priority) 
  {
    super(priority);
  }

  public TestStack() {}

  public void JNTMain()
  {
    deb.debug("Starting Stack");

    TCP tcp = new TCP();
    IP ip = new IP();
    SocketAdapter sa = new SocketAdapter();
    
    try 
    {
      JNTModuleManager.Register(tcp, "TCP");
      JNTModuleManager.Register(ip, "IP");
      JNTModuleManager.Register(sa, "SA");
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (Exception e)
    {
      deb.error("System exception: " + e.getMessage());
    }
  }

  public static void main(String args[])
  {  	
    try
    {
      int priority = Integer.parseInt(args[0]);      
      new TestStack(priority).Start();
    }
    catch (JNTException e) { deb.error(e.GetError()); }
    catch (Exception e) 
    {
      try { new TestStack().Start(); }
      catch (JNTException w) { deb.error(w.GetError()); }
    }
  }
}
