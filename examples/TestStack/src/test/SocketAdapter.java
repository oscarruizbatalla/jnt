package test;

import org.apache.log4j.*;

import org.orb.jnt.JNTAdapter.JNTSocketAdapter;
import org.orb.jnt.JNTModule.*;
import org.orb.jnt.JNTQueue.*;
import org.orb.jnt.JNTTimer.*;
import org.orb.jnt.JNTError.*;
import org.orb.jnt.JNTSDU.*;


class SocketAdapter extends JNTSocketAdapter {

  public SocketAdapter() {}

  // one queue
  public JNTQueue ipQueue;
  
  public JNTModule myOID;
  public JNTModule ip;

  // debugger
  static Logger deb = Logger.getLogger(SocketAdapter.class.getName());

  // **************************************************
  //
  //  Object management
  //
  // **************************************************

  public void Initialize()
  {
    deb.debug("Initialize");
    
    // open top queue
    ip = JNTModuleManager.GetModule("IP");
    myOID = this;
    
    try 
    {
      JNTQueueManager.Open(ip, myOID);
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
  }
  
  public void Finalize()
  {
    deb.debug("Finalize");
  }

  // **************************************************
  //
  //  Timers management
  //
  // **************************************************

  public void TimerExpired(JNTTimer timer)
  {
    deb.debug("TimerExpired " + timer.GetTimeout());
  }

  // **************************************************
  //
  //  Top Queue management
  //
  // **************************************************
 
  public void TopQueueOpened(JNTQueue queue)
  {
    deb.debug("TopQueueOpened");
    queue.Accept();
  }

  public void TopQueueClosed(JNTQueue queue)
  {
    deb.debug("TopQueueClosed");
  }

  public void TopQueueAccepted(JNTQueue queue)
  {
    deb.debug("TopQueueAccepted");
    
    try
    {
      ipQueue = queue;
      AcceptConnection(8888);
    }
    catch (JNTException e)
    {
      deb.debug("Cannot accept connections on : " + e.GetError());
    }
  }

  public void TopQueueRejected(JNTQueue queue)
  {
    deb.debug("TopQueueRejected");
  }

  public void TopQueueService(JNTQueue queue)
  {
    deb.debug("TopQueueService");

    int type = 0;
    int size = 0;
    byte dst[] = null;
    JNTSDU sdu = null;

    try
    {
      // receive phase
      // ---------------------------------------------
      while (true)
      {
        try
        {
          sdu = queue.Receive();
        }
        catch (JNTQueueEmptyException re)
        {
          deb.warn(re.GetError());
          break;
        }
        catch (JNTException e) { throw e; }
        catch (JNTRuntimeException re) { throw re; }

        size = sdu.GetSize();
        type = sdu.GetType();

        dst = new byte[size];

        sdu.RemoveHead(dst, size);

        deb.info("Receiving sdu type: " + type);
        deb.info("Receiving sdu data: " + new String(dst));

        JNTSDUManager.Destroy(sdu);
      
        // send it to socket
        // ---------------------------------------------
        //byte[] result = new String("Test de envio").getBytes();
        //Write(dst);        
      } // while end
    } // try end
    catch (JNTNoMemoryException e)
    {
      deb.fatal(e.GetError());
      return;
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
    catch (JNTRuntimeException re)
    {
      deb.error(re.GetError());
    }
  }
  
  public void ConnectionService(byte[] message)
  {
    deb.debug("ConnectionService");
    
    try
    {
      // lets send an sdu to TCP type = 0
      // -------------------------------------------------
      JNTSDU sdu = JNTSDUManager.New(); 

      // sdu data and type      
      int type = 0;
      
      deb.info("Sending sdu type: " + type);
      deb.info("Sending sdu data: " + new String(message));
 
      sdu.SetType(type);
      sdu.AddTail(message, message.length);

      ipQueue.Send(sdu); 
    }
    catch (JNTException e)
    {
      deb.error(e.GetError());
    }
  }
  
  public void ClientConnectionOpened()
  {
    deb.debug("ClientConnectionOpened()");
  }
  
  public void ClientConnectionClosed()
  {
    deb.debug("ClientConnectionClosed()");
  }
  
  public void ServerConnectionClosed()
  {
    deb.debug("ServerConnectionClosed()");
  }
  
  public void ServerConnectionAccepted()
  {
    deb.debug("ServerConnectionAccepted()");
  }
  
  public void ServerConnectionRejected()
  {
    deb.debug("ServerConnectionRejected()");
  }

}